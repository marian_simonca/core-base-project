﻿using System.Collections.Generic;
using Fabrit.CBP.Data;
using Fabrit.CBP.Data.Entities;
using Fabrit.CBP.Repository.Contracts;

namespace Fabrit.CBP.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly MyDbContext _context;

        public GenericRepository(MyDbContext context)
        {
            _context = context;
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public T GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public T Update(T property)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}