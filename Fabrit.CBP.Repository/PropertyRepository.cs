﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fabrit.CBP.Data;
using Fabrit.CBP.Data.Entities;
using Fabrit.CBP.Repository.Contracts;

namespace Fabrit.CBP.Repository
{
    public class PropertyRepository : GenericRepository<Property>, IPropertyRepository
    {
        // todo: use the context to access your database
        // e.g. _context.Properties.FirstOrDefault( ... ); <=> _context.Set<Property>().FirstOrDefault( ... )
        // e.g. _context.Properties.Add(property); <=> _context.Set<Property>().Add(property)


        public PropertyRepository(MyDbContext context) : base(context) { }

//        public void GetBetweenDates(DateTime a, DateTime b)
//        {
//            var properties = GetAll()
//        }
    }
}