﻿using System.Collections.Generic;
using Fabrit.CBP.Business;
using Fabrit.CBP.Business.Contracts;
using Fabrit.CBP.Data;
using Fabrit.CBP.Data.Entities;

namespace CoreBaseProject.Controllers
{


    public class PropertyController
    {
        private readonly IPropertyManager _propertyManager;

        public PropertyController(IPropertyManager propertyManager)
        {
            _propertyManager = propertyManager;
        }

        public IEnumerable<Property> GetAll()
        {
            return _propertyManager.GetAll();
        }

        // todo: write your code here
    }
}