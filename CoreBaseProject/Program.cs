﻿
namespace CoreBaseProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var startup = new Startup();
            startup.Setup();
        }
    }
}
