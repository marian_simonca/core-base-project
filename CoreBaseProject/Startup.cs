﻿using CoreBaseProject.Controllers;
using Fabrit.CBP.Business;
using Fabrit.CBP.Data;
using Fabrit.CBP.Repository;

namespace CoreBaseProject
{
    public class Startup
    {
        public void Setup()
        {
            var context = new MyDbContext();
            var repo = new PropertyRepository(context);
            var manager = new PropertyManager(repo);

            var propController = new PropertyController(manager);
        }
    }
}