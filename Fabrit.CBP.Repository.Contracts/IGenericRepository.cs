﻿using System.Collections.Generic;
using Fabrit.CBP.Data.Entities;

namespace Fabrit.CBP.Repository.Contracts
{
    public interface IGenericRepository<T>
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        T Update(T entity);

        void Remove(int id);
    }
}