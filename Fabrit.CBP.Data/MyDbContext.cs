﻿using Fabrit.CBP.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Fabrit.CBP.Data
{
    public class MyDbContext : DbContext
    {
        // todo: add DbSets here

        public virtual DbSet<Property> Properties { get; set; }
    }
}