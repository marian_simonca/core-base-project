﻿using System.Collections.Generic;
using Fabrit.CBP.Data.Entities;

namespace Fabrit.CBP.Business.Contracts
{
    public interface IPropertyManager
    {
        IEnumerable<Property> GetAll();

        Property GetById(int id);

        Property Update(Property property);

        void Remove(int id);
    }
}