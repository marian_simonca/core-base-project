﻿namespace Fabrit.CBP.Data.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }

        // todo: extend all your entities from this
    }
}