﻿using System.Collections.Generic;

namespace Fabrit.CBP.Data.Entities
{
    public class User : BaseEntity
    {
        public IList<Property> Properties { get; set; }
    }
}